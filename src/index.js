import React from "react";
import ReactDOM from "react-dom";
import "../css/app.css";
import Animation from "./components/Animation";
import Project from "./components/Project";
import About from "./components/About";
import Footer from "./components/Footer";
import Cookies from "js-cookie";

function App() {
  const themeDefault = document.body.classList.add("dark");
  Cookies.set("theme", "themeDefault");
  const projectsList = [
    {
      image: require("./img/1.png"),
      link: "http://editagud.com/instagram/index.php",
      description:
        "Project consisted of creating the Instagram using PHP and MongoDB ",
      contribution:
        "I created all UX and UI design. The main purpose of the project is to show my knowledge of databases which was improved through practice. Although my “Instagram” project is only a prototype and learning tool, it implements the CRUD pattern with PHP implementation. With phpMyAdmin tool I have structured and normalized the database and find the solutions for the problems. "
    },
    {
      image: require("./img/2.png"),
      link: "https://ticketbutler.io/en/",
      description: "Ticketbutler homepage",
      contribution:
        "Maintaining and improving page with GatbyJS. Currently rewriting website to Reactjs and Parsel."
    },
    {
      image: require("./img/8.png"),
      link: "https://e.ticketbutler.io/en/",
      description: "Ticketbutler dashboard",
      contribution: "Building features with React library."
    },

    {
      image: require("./img/7.png"),
      link: "http://editagud.com/bike_rental/",
      description: "Bike rental website",
      contribution:
        "This was only prototype created with Javascript, Json and PHP."
    },

    {
      image: require("./img/4.png"),
      link: "http://editagud.com/avart/index.html",
      description:
        "The project consists of the online solution (a dynamic website) including a digital media strategy with examples of content, and a report with numbered pages, and references to the relevant sources.",
      contribution:
        "Web design (logo, design layuot, wireframes), Interaction (HTML, CSS, JavaScript, JSON, GreenSock animation), WordPress API, SVG, Use of digital media (presentational video)."
    },
    {
      image: require("./img/3.png"),
      link: "http://editagud.com/bank/",
      description: "The project was to create bank system website using PHP.",
      contribution:
        "The bank website was created by the school project. The main purpose was to send and get transfers, make admin panel that user can block bank users, user is able to create new bank cards and accounts, login, signup with email confirmation. All code written with PHP. "
    },

    {
      image: require("./img/5.png"),
      link: "http://editagud.com/professional%20profile/index.html",
      description: "Old my portfolio website",
      contribution:
        "The old portfolio I used searchin internship in Multimedia design studies. All website written basicly only in Javascript, Html and CSS."
    },
    {
      image: require("./img/6.png"),
      link: "http://editagud.com/restaurantsite/index.html",
      description: `The project is about creating a dynamic website for the imaginary restaurant "Petrograd". The site show data from the service, and dynamically reflect the state of the data, meaning, if a product is marked as “sold out”, the website should reflect that. The project is intended to create a design, adjust it to dynamic, unknown data, and implement the code to show the dynamic content.`,
      contribution:
        "Web design (logo, design layuot, wireframes), Interaction (HTML, CSS, JavaScript, JSON)"
    }
  ];

  const workRef = React.useRef(null);
  const aboutRef = React.useRef(null);
  const contactRef = React.useRef(null);

  const list = [
    {
      link: "#about",
      label: "About",
      ref: aboutRef
    },
    {
      link: "#work",
      label: "Work",
      ref: workRef
    },
    {
      link: "#contact",
      label: "Contact",
      ref: contactRef
    }
  ];
  return (
    <div>
      <div className="hero">
        <div className="container"></div>

        <header>
          <nav
            className="navbar"
            role="navigation"
            aria-label="main navigation"
          >
            <div className="navbar-container">
              {list.map((item, index) => {
                return (
                  <a
                    key={index}
                    className="navbar-item"
                    onClick={function() {
                      if (item.ref.current) {
                        item.ref.current.scrollIntoView({
                          behavior: "smooth",
                          block: "start"
                        });
                      }
                    }}
                  >
                    {item.label}
                  </a>
                );
              })}

              <div className="theme-toggle" id="switchButton">
                <input
                  id="theme"
                  type="checkbox"
                  onChange={() => {
                    const nextTheme =
                      Cookies.get("theme") === "light" ? "dark" : "light";
                    document.body.classList.toggle("light");
                    document.body.classList.toggle("dark");
                    Cookies.set("theme", nextTheme);
                  }}
                />
                <label htmlFor="theme"> </label>
                <span className="on">🌙</span>
                <span className="off">☀️</span>
              </div>
            </div>
          </nav>
        </header>
        <div className="socialIcons">
          <a
            href="https://www.linkedin.com/in/edita-gudan-459322144/"
            target="_blank"
          >
            <img src={require("./icons/linkedin.png")} />
          </a>
          <a href="https://gitlab.com/Editagud" target="_blank">
            <img src={require("./icons/gitlab.png")} style={{ width: 40 }} />
          </a>
          <a href="mailto:edita.gudan@gmail.com">
            <img src={require("./icons/email.png")} style={{ width: 35 }} />
          </a>
        </div>

        <div
          style={{
            margin: "0 auto",
            overflow: "unset",
            height: "80vh",
            width: "100vw",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          {" "}
          <Animation />
          <a
            onClick={function() {
              aboutRef.current.scrollIntoView({
                behavior: "smooth",
                block: "start"
              });
            }}
          >
            <span></span>
          </a>
        </div>
      </div>
      <section ref={aboutRef} id="about">
        <h1>ABOUT ME.</h1>
        <About />
      </section>
      <section ref={workRef} id="work">
        <h1>PROJECTS.</h1>
        <div className="imagesListDisplay">
          {projectsList.map((project, index) => {
            return (
              <Project
                key={index}
                link={project.link}
                image={project.image}
                description={project.description}
                contribution={project.contribution}
              ></Project>
            );
          })}
        </div>
      </section>
      <div ref={contactRef} id="contact">
        <Footer />
      </div>
    </div>
  );
}

let rootElement = document.getElementById("app");

ReactDOM.render(<App name="Edita" />, rootElement);
