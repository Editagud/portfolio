import React from "react";
import Lottie from "react-lottie";
import * as animationData from "../../gudan.json";

export default class Animation extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice"
      }
    };

    return (
      <div>
        <Lottie options={defaultOptions} height={400} width={600} />
      </div>
    );
  }
}
