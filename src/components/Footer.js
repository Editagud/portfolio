import React from "react";
import "../../css/app.css";

const Header = () => (
  <div id="footer">
    <div id="footerLinks">
      <a
        href="https://www.linkedin.com/in/edita-gudan-459322144/"
        target="_blank"
      >
        Linkedin
      </a>
      <a href="https://gitlab.com/Editagud" target="_blank">
        Gitlab
      </a>
    </div>
    <div style={{ marginTop: "5%" }}>
      <p>Feel free to reach out at</p>
      <a href="mailto:edita.gudan@gmail.com">edita.gudan@gmail.com</a>
    </div>
    <div className="footerText">
      <p>Made with ❤️️ by Edita Gudan</p>
    </div>
  </div>
);

export default Header;
