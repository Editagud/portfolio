import React from "react";
import "../../css/app.css";
import { Modal } from "../components/Modal";

export default function Project({ image, link, description, contribution }) {
  const [open, setOpen] = React.useState(false);
  return (
    <div>
      <div className="post">
        <div
          className="backgroundImage"
          style={{ backgroundImage: `url(${image})` }}
        ></div>

        <div className="image">
          <div id="imageEmotionDisplay">
            <img className="images" src={image} alt="image" />
          </div>
        </div>

        <div className="imageHover">
          <button
            className="moreInfoButton"
            onClick={() => {
              setOpen(true);
            }}
          >
            More info >>>
          </button>
        </div>
      </div>
      <Modal
        isOpen={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <div>
          <div className="projectContainer">
            <div className="projectImageContainer">
              <img className="images" src={image} alt="image" />
            </div>
            <div className="projectInfo">
              <h2>Project description</h2>
              <p>{description}</p>
              <h2> My contribution</h2>
              <p>{contribution}</p>
            </div>
          </div>
          <a href={link} target="_blank" rel="noopener noreferrer">
            <button className="moreInfoButton" style={{ marginBottom: "30px" }}>
              See it live >>>
            </button>
          </a>
        </div>
      </Modal>
    </div>
  );
}
