import React from "react";
import "../../css/app.css";
import pdf from "../cv.pdf";

export default function About() {
  return (
    <div class="aboutContainer">
      <div className="aboutInfo">
        <div className="about1container">
          <div id="profile-image">
            <img src={require("../img/portrait.jpg")} alt="profile"></img>
          </div>
        </div>
        <div className="about2container">
          <p id="aboutText" style={{ textAlign: "justify", fontSize: "1.1em" }}>
            Hi, I am Edita Gudan, a 26-year-old Lithunian Web developer living
            in Copenhagen. Curently I am gaining a Top-up degree of Web
            Development at KEA (Københavns Erhvervsakademi). At the moment I am
            working mostly with React library. The background of skills includes
            HTML, CSS, Javascript, PHP, Gatsby, Git and MariaDB with a learning
            stage of Django and Node.js. I am thriving to always learn, keep
            improving and challenge myself. Futhermore, I am passionate about
            technology, code, art, design, traveling and creativity.
          </p>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginBottom: 20
            }}
          >
            <img className="icons" src={require("../icons/react.png")} />
            <img
              className="icons"
              style={{ width: 40 }}
              src={require("../icons/js.png")}
            />
            <img className="icons" src={require("../icons/node.png")} />
            <img
              className="icons"
              style={{ width: 40 }}
              src={require("../icons/php.png")}
            />
            <img
              className="icons"
              style={{ width: 40 }}
              src={require("../icons/gatsby.png")}
            />
            <img
              className="icons"
              style={{ width: 50 }}
              src={require("../icons/django.png")}
            />
            <img
              className="icons"
              style={{ width: 70 }}
              src={require("../icons/design.png")}
            />
          </div>
        </div>
      </div>
      <a href={pdf} target="_blank" rel="noopener noreferrer">
        <button id="downloadButton" className="moreInfoButton">
          {" "}
          Download CV
        </button>
      </a>
    </div>
  );
}
